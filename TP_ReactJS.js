Go to https://jsfiddle.net

add : https://unpkg.com/react-dom@17.0.1/umd/react-dom.development.js
https://unpkg.com/react@17.0.1/umd/react.development.js

run each project :

-------------------------------------------------------------------


function App() {
  const integers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
  const isEven = n => n%2 === 0
  const mapIntToDiv = n => {
  	const styleObject = {color: isEven(n) ? 'red' : 'blue'}
    return < div style = {styleObject}> {n} < /div>
  }
  const divs = integers.map(mapIntToDiv)
  return <div > {divs} < /div>

  }

ReactDOM.render( 
<App / > ,
  document.getElementById('root')
);



-------------------------------------------------------------------


function App() {


  const [name, setName] = React.useState('bite')
  return <div > 
    {name} 
    <button onClick = {() => alert(name)> Alerte </button> 
    <input value={name} onChange={(e) => setName(e.target.value)} />
    </div>
}

ReactDOM.render( <
  App / > ,
  document.getElementById('root')
);




-------------------------------------------------------------------




function App() {
  function handleChangeName(e) {
    setName(e.target.value);
  }

  const [name, setName] = React.useState('')
  return <div >
    <
    input type = "text"
  value = {
    name
  }
  onChange = {
    handleChangeName
  }
  /> <h1>Bonjour {
  name
} < /h1> < /
div >

}

ReactDOM.render( <
  App / > ,
  document.getElementById('root')
);


-------------------------------------------------------------------



function Counter() {
  const [count, setCount] = React.useState(0);

  const handleIncrement = () => {
    setCount(prevCount => prevCount + 1);
  };

  const handleDecrement = () => {
    setCount(prevCount => prevCount - 1);
  };
  
  function handleChangeInput(e) {
    setWord(e.target.value);
  }
  
  function reverseString(word) {
    return word.split("").reverse().join("");
}
  
  const [word, setWord] = React.useState('')
 
  return (
    <div>
      <div>
        <button onClick={handleDecrement}>-</button>
        <button onClick={handleIncrement}>+</button>
      </div>
      <div>Count is {count}</div>
      <h3>Type something...</h3>
      <input value={word} onChange={handleChangeInput}></input>
      <h5>{reverseString(word)}</h5>
    </div>
  );
}

ReactDOM.render( <
  Counter / > ,
  document.getElementById('root')
);

